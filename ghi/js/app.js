function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="col py-3 px-lg-3">
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body>
          <h5 class="card-title bold">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer bg-transparent border-success">
            <small> ${start} - ${end}</small>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error("Response not ok");
      } else {
        const data = await response.json();
        console.log(data)

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {

            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name
            const startDate = new Date(details.conference.starts);
            const start = startDate.getMonth() + "/" + startDate.getDate() + "/" + startDate.getFullYear()

            const endDate = new Date(details.conference.ends);
            const end = endDate.getMonth() + "/" + endDate.getDate() + "/" + endDate.getFullYear()

            const html = createCard(name, description, pictureUrl, start, end, location);
            const column = document.querySelector(".row");
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error("error", error)
    }

  });
